export default {
    state: {
        isLoaded: false,
        goods: []
    },
    mutations: {
        addGood(state,payload){
            state.goods.push(payload)
            localStorage.setItem('testAppGoodsList',JSON.stringify(state.goods))
        },
        removeGood(state,id){
            state.goods = state.goods.filter(item => {
                return item.id !== id
            })
            localStorage.setItem('testAppGoodsList',JSON.stringify(state.goods))
        },
        setGoods(state,payload){
            state.goods = payload
            state.isLoaded = true
        },
        sortGoods(state,sortBy){
            state.goods.sort((a,b)=>a.id<b.id?-1:1)

            if(sortBy == 'min'){ state.goods.sort((a,b)=>+a.price.replace(/[^0-9,]/g,'')<+b.price.replace(/[^0-9,]/g,'')?-1:1)}
            else if(sortBy == 'max'){ state.goods.sort((a,b)=>+a.price.replace(/[^0-9,]/g,'')>+b.price.replace(/[^0-9,]/g,'')?-1:1)}
            else if(sortBy == 'name'){ state.goods.sort((a,b)=>a.name<b.name?-1:1)}
        }
    },
    getters: {
        allGoods(state){
            return state.goods
        },
        isGoodsLoaded(state){
            return state.isLoaded
        }
    },
    actions: {
        loadGoods(ctx){
            let savedData = JSON.parse(localStorage.getItem("testAppGoodsList"))||[]
            ctx.commit('setGoods',savedData)
        }
    },
}